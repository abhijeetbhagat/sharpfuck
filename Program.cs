﻿/*  Copyright 2013 abhijeet bhagat
 


    SharpFuck is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SharpFuck is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SharpFuck.  If not, see <http://www.gnu.org/licenses/>.
 */


/* 
Hello world 1: +++++ +++++ [>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.
Hello world 2: >+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.>+++++++++++[<+++++>-]<.>++++++++[<+++>-]<.+++.------.--------.[-]>++++++++[<++++>-]<+.[-]++++++++++. 
*/

using System;

namespace SharpFuck
{
    class Program
    {
        static void Main(string[] args)
        {
            string bfSource = ">+++++++++++[<++++++++>-]<-.>++++[<++++>-]<+.-------.>+++++[<++++>-]<-.[-]>++++++++[<++++>-]<.>+++++++++[<++++++++>-]<+.++++++++++.[-]>++++++++[<++++>-]<.>+++++++++++[<++++++++>-]<+.----------.++++++.---.[-]>++++++++[<++++>-]<.>+++++++++++++[<++++++>-]<.-------------.++++++++++++.--------.>++++++[<------>-]<--.[-]+++++++++++++.---.[-]+[>,-------------]>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.[-]<-[<-]>[++++++++++++++.>]++++++++++."; ;
            int[] tape = new int[30000];
            for (int i = 0, j = 0, t = 0; i < bfSource.Length; i++)
            {
                switch (bfSource[i])
                {
                    case '>':
                        j++;
                        break;
                    case '<':
                        j--;
                        break;
                    case '+':
                        tape[j]++;
                        break;
                    case '-':
                        tape[j]--;
                        break;
                    case '.':
                        System.Console.Write((char)tape[j]);
                        break;
                    case ',':
                        tape[j] = System.Console.Read();
                        break;
                    case '[':
                        t = i;
                        if (tape[j] == 0)
                        {
                            while (bfSource[i++] != ']' && i < bfSource.Length) ;
                            i--; //set i to point to ']' because the loop's i++ will set i to point to the char after ']'
                        }
                        break;
                    case ']':
                        i = t-1; //set it to one char before '[' because the loop's i++ will set i to point to '['
                        break;
                    default:
                        break;
                }
            }

            Console.ReadKey();
        }
    }
}
